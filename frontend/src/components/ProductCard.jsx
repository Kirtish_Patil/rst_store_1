import { Box, Flex, Heading, Image, Link, Text } from "@chakra-ui/react";
import Rating from "./Rating";
import { Link as RouterLink } from "react-router-dom";

const ProductCard = ({ product }) => {
  return (
    <>
      <Link as={RouterLink} to={`/product/${product._id}`} _hover={{ textDeecor: "none" }}>
        <Box borderRadius="lg" bgColor="white" _hover={{ shadow: "md" }}>
          <Image
            src={product.image}
            alt={product.name}
            w="full"
            h="430px"
            objectFit='contain'
            borderTopLeftRadius="lg"
            borderTopRightRadius="lg"
          />
          <Flex px="4" direction="column" justifyContent="space-between">
            <Heading as="h4" fontSize="xl" fontWeight="semibold">
              {product.name}
            </Heading>
            <Flex py="1.5" justifyContent="space-between" alignItems="center">
              <Rating value={product.rating} />
              <Text>₹{product.price}</Text>
            </Flex>
          </Flex>
        </Box>
      </Link>
    </>
  );
};

export default ProductCard;
