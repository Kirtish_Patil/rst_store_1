import { Box, Flex, Icon, Text } from "@chakra-ui/react";
import {ImStarHalf, ImStarEmpty, ImStarFull} from 'react-icons/im'

const Rating = ({ value, text }) => {
  return (
    <Flex alignItems="center" >
      <Box mr='2'>
      <Icon
        as={value >= 1 ? ImStarFull : value >= 0.5 ? ImStarHalf : ImStarEmpty}
        color='yellow.600'
      />
      <Icon
        as={value >= 2 ? ImStarFull : value >= 1.5 ? ImStarHalf : ImStarEmpty}
        color='yellow.600'
      />
      <Icon
        as={value >= 3 ? ImStarFull : value >= 2.5 ? ImStarHalf : ImStarEmpty}
        color='yellow.600'
      />
      <Icon
        as={value >= 4 ? ImStarFull : value >= 3.5 ? ImStarHalf : ImStarEmpty}
        color='yellow.600'
      />
      <Icon
        as={value >= 5 ? ImStarFull : value >= 4.5 ? ImStarHalf : ImStarEmpty}
        color='yellow.600'
      />
      </Box>
      <Text>{text}</Text>
    </Flex>
  );
};

export default Rating;
