import { Link } from "@chakra-ui/react";
import { Link as RouterLink } from "react-router-dom";

const HeaderMenuItem = ({ icon, label, url }) => {
  return (
    <Link
      as={RouterLink}
      to={url}
      fontSize="sm"
      letterSpacing="wide"
      textTransform="capitalize"
      mr="5"
      display="flex"
      alignItems="center"
      color="white"
      mb={{ base: "2", md: 0 }}
      _hover={{textDecor: 'none', transition: "0.2s", color: 'blue.700' }}
    >
      {icon}
      {label}
    </Link>
  );
};

export default HeaderMenuItem;
