import { Grid, Heading, Text } from "@chakra-ui/react";
import ProductCard from "../components/ProductCard";
import { listProducts } from "../actions/productActions";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import Loader from "../components/Loader";
import Message from "../components/Message";

const HomeScreen = () => {
  const completeState = useSelector((state) => state)
	// console.log(completeState)
  const dispatch = useDispatch()

  const productList = useSelector((state) => state.productList)
  const {loading, error, products} = productList

  useEffect(() => {
	dispatch(listProducts())
  }, [dispatch])

  return (
    <>
      <Heading as="h2" mb="8" fontSize="xl">
        Latest Products
      </Heading>

      {loading ? (
		<Loader />
	  ) : error ? (
		<Message type="error">{error}</Message>	
	  ) : (
		<Grid templateColumns="1fr 1fr 1fr 1fr" gap="8">
        {products.map((prod) => (
          <ProductCard key={prod._id} product={prod} />
        ))}
      </Grid>
	  )}
    </>
  );
};

export default HomeScreen;



