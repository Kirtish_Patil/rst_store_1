import {
  Button,
  Flex,
  Grid,
  Heading,
  Icon,
  Image,
  Text,
  Select,
  Box,
  FormControl,
  FormLabel,
  Textarea,
} from "@chakra-ui/react";
import { Link as RouterLink, useNavigate, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { IoIosArrowBack } from "react-icons/io";
import Rating from "../components/Rating";
import { useDispatch, useSelector } from "react-redux";
import {
  createProductReview,
  listProductDetails,
} from "../actions/productActions";
import Loader from "../components/Loader";
import Message from "../components/Message";
import { PRODUCT_REVIEW_CREATE_RESET } from "../constants/productConstants";

const SinProductScreen = () => {
  // const completeState = useSelector((state) => state)
  // console.log(completeState)
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { id } = useParams();
  const [qty, setQty] = useState(1);
  const [rating, setRating] = useState(0);
  const [comment, setComment] = useState("");

  const produtDetails = useSelector((state) => state.productDetails);
  const { loading, error, product } = produtDetails;

  const productReviewCreate = useSelector((state) => state.productReviewCreate);
  const {
    loading: loadingReview,
    success: successReview,
    error: errorReview,
  } = productReviewCreate;

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  useEffect(() => {
    if (successReview) {
      alert("review submitted");
      setRating(0);
      setComment("");
      dispatch({ type: PRODUCT_REVIEW_CREATE_RESET });
    }

    dispatch(listProductDetails(id));
  }, [dispatch, id, successReview]);

  const addToCartHandler = () => {
    navigate(`/cart/${id}?qty=${qty}`);
  };

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(createProductReview(id, { rating, comment }));
  };

  return (
    <>
      <Flex mb="5">
        <Button
          as={RouterLink}
          to="/"
          color="white"
          bgColor="cyan.600"
          size="sm"
          _hover="none"
        >
          <Icon as={IoIosArrowBack} />
        </Button>
      </Flex>

      {loading ? (
        <Loader />
      ) : error ? (
        <Message type="error">{error}</Message>
      ) : (
        <>
          <Grid templateColumns="3fr 4fr 3fr">
            {/* Column 1 */}

            <Image src={product.image} alt={product.name} borderRadius="2xl" />

            {/* Column 2  */}

            <Flex direction="column" px="6">
              <Heading as="h5" fontSize="base" color="gray.500">
                {product.brand}
              </Heading>

              <Heading as="h2" fontSize="4xl" mb="2">
                {product.name}
              </Heading>

              <Rating
                value={product.rating}
                text={`${product.numReviews} reviews`}
              />

              <Heading
                as="h5"
                fontSize="4xl"
                fontWeight="bold"
                color="teal.600"
                my="3"
              >
                ₹{product.price}
              </Heading>

              <Text>{product.description}</Text>
            </Flex>

            {/* Column 3 */}
            <Flex direction="column">
              <Flex justifyContent="space-between" py="2">
                <Text>Price:</Text>
                <Text fontWeight="bold">₹{product.price}</Text>
              </Flex>

              <Flex justifyContent="space-between" py="2">
                <Text>Status:</Text>
                <Text fontWeight="bold">
                  {product.countInStock > 0 ? (
                    <Text color="green">InStock</Text>
                  ) : (
                    <Text color="red.600">Not available</Text>
                  )}
                </Text>
              </Flex>

              {product.countInStock > 0 && (
                <Flex justifyContent={"space-between"} py="2">
                  <Text>Qty:</Text>
                  <Select
                    value={qty}
                    onChange={(e) => setQty(e.target.value)}
                    width="30%"
                  >
                    {[...Array(product.countInStock).keys()].map((i) => (
                      <option key={i + 1}>{i + 1}</option>
                    ))}
                  </Select>
                </Flex>
              )}
              <Button
                bgColor="cyan.600"
                color="white"
                my="2"
                textTransform="uppercase"
                letterSpacing="wide"
                _hover="none"
                isDisabled={product.countInStock === 0}
                onClick={addToCartHandler}
              >
                Add to Cart
              </Button>
            </Flex>
          </Grid>

          {/* Review */}
          <Box
            p="10"
            bgColor="white"
            rounded="md"
            mt="10"
            borderColor="gray.300"
          >
            <Heading as="h3" size="lg" mb="6">
              Write a Review
            </Heading>
            {product.reviews.length === 0 && <Message>No Reviews</Message>}

            {product.reviews.length !== 0 && (
              <Box p="4" bgColor="white" rounded="md" mb="1" mt="5">
                {product.reviews.map((review) => (
                  <Flex direction="column" key={review.id} mb="5">
                    <Flex justifyContent="space-between">
                      <Text fontSize="lg">
                        <strong>{review.name}</strong>
                      </Text>
                      <Rating value={review.rating} />
                    </Flex>
                    <Text mt="2">{review.comment}</Text>
                  </Flex>
                ))}
              </Box>
            )}

            {errorReview && <Message type="error">{errorReview}</Message>}

            {userInfo ? (
              <form onSubmit={submitHandler}>
                <FormControl id="rating" mb="3">
                  <FormLabel>Rating</FormLabel>
                  <Select
                    placeholder="Select Option"
                    value={rating}
                    onChange={(e) => setRating(e.target.value)}
                  >
                    <option>Select...</option>
                    <option value="1">1 - Poor</option>
                    <option value="2">2 - Okay</option>
                    <option value="3">3 - Good</option>
                    <option value="4">4 - Very Good</option>
                    <option value="5">5 - Excellent</option>
                  </Select>
                  <FormControl id="comment" mb="3">
                    <FormLabel>Comment</FormLabel>
                    <Textarea
                      value={comment}
                      onChange={(e) => setComment(e.target.value)}
                    ></Textarea>
                  </FormControl>
                  <Button bgColor='cyan.600' color='white' type="submit">
                    Post Review
                  </Button>
                </FormControl>
              </form>
            ) : (
              <Message>Please login to write a review</Message>
            )}
          </Box>
        </>
      )}
    </>
  );
};

export default SinProductScreen;
